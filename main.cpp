#include <iostream>

using namespace std;

class Time{
public:
    Time(){
        hours = 0;
        minutes = 0;
        seconds = 0;
    }
    Time(int _hours, int _minutes, int _seconds){
        hours = _hours;
        minutes = _minutes;
        seconds = _seconds;
    }


    int getHours() {return hours;}
    int getMinutes() {return minutes;}
    int getSeconds() {return seconds;}


    private:
        int hours;
        int minutes;
        int seconds;

};

int main(){
    Time now(8,56,20);
    cout << "The time is " << now.getHours() << ":" << now.getMinutes() << ":" << now.getSeconds();

    return 0;
}
